## ISO Builder

This ISO builder is fork of the good work from the elementary crew.  Many thanks to the devs there https://github.com/elementary/os

## Building Locally

The following example uses Docker and assumes you have Docker correctly installed and set up:

Clone this project & cd into it:

    git clone https://gitlab.com/tile-os/iso-builder && cd iso-builder

Configure the channel in the etc/terraform.conf (`dev`, `stable`).
Configure desktop by export `DESKTOP` variable. May be a `river` or `sway`. 

Run the build for Sway:

    docker run --privileged -i -v /proc:/proc \
        -v ${PWD}:/working_dir \
        -w /working_dir \
        -e DESKTOP=sway \
        debian:latest \
        /bin/bash -s etc/terraform.conf < build.sh

When done, your image will be in the `builds` folder.
